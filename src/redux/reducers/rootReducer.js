import { combineReducers } from "redux";

import reducerBoard from "./reducerBoard";
import reducerList from "./reducerList";

const rootReducer = combineReducers({
  reducerBoard,
  reducerList,
});

export default rootReducer;
