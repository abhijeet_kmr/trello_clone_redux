import * as actionTypes from "../constants";

const initialState = {
  boards: [],
  isLoading: true,
  isError: false,
};

const reducerBoard = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.GET_ALL_BOARDS:
      return {
        ...state,
        isLoading: false,
        isError: false,
        boards: action.data,
      };
    case actionTypes.GET_ALL_BOARDS_FAIL:
      return {
        ...state,
        isLoading: false,
        isError: "Failed to load boards",
        boards: [],
      };
    case actionTypes.CREATE_NEW_BOARD:
      const newBoardData = [...state.boards, action.data];
      return {
        ...state,
        isLoading: false,
        isError: false,
        boards: newBoardData,
      };
    case actionTypes.CREATE_NEW_BOARD_FAIL:
      return {
        ...state,
        isLoading: false,
        isError: "Failed to create new board",
        boards: action.data,
      };

    default:
      return state;
  }
};

export default reducerBoard;
