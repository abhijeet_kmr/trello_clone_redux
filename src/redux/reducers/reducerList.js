import * as actionTypes from "../constants";

const initialList = {
  allLists: [],
  isLoading: true,
  isError: false,
};

const reducerList = (state = initialList, action) => {
  switch (action.type) {
    case actionTypes.GET_ALL_LISTS:
      return {
        ...state,
        isLoading: false,
        isError: false,
        allLists: action.data,
      };
    case actionTypes.GET_ALL_LISTS_FAIL:
      return {
        ...state,
        isLoading: false,
        isError: "Failed to load Lists",
        allLists: null,
      };
    case actionTypes.CREATE_A_LIST:
      const newListData = [...state.allLists, action.data];
      return {
        ...state,
        isOpen: false,
        showForm: false,
        isError: false,
        allLists: newListData,
      };
    case actionTypes.CREATE_A_LIST_FAIL:
      return {
        ...state,
        isError: "Failed to create new list",
        allLists: null,
      };
    case actionTypes.DELETE_A_LIST:
      const newListDataPostDelete = state.allLists.filter(
        (item) => item.id !== action.id
      );
      return {
        ...state,
        isError: false,
        allLists: newListDataPostDelete,
      };
    default:
      return state;
  }
};

export default reducerList;
