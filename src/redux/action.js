import * as actionTypes from "./constants";

export const getBoardDataSuccess = (data) => {
  return {
    type: actionTypes.GET_ALL_BOARDS,
    data: data,
  };
};

export const getBoardDataFail = (data) => {
  return {
    type: actionTypes.GET_ALL_BOARDS_FAIL,
    data: data,
  };
};

export const createBoardSuccess = (data) => {
  return {
    type: actionTypes.CREATE_NEW_BOARD,
    data: data,
  };
};

export const createBoardFail = (data) => {
  return {
    type: actionTypes.CREATE_NEW_BOARD_FAIL,
    data: data,
  };
};

export const getAllLists = (data) => {
  return {
    type: actionTypes.GET_ALL_LISTS,
    data: data,
  };
};

export const getAllListsFail = (data) => {
  return {
    type: actionTypes.GET_ALL_LISTS_FAIL,
    data: data,
  };
};

export const createNewList = (data) => {
  return {
    type: actionTypes.CREATE_A_LIST,
    data: data,
  };
};

export const createNewListFail = (data) => {
  return {
    type: actionTypes.CREATE_A_LIST_FAIL,
    data: data,
  };
};

export const deleteList = (data, id) => {
  return {
    type: actionTypes.DELETE_A_LIST,
    data: data,
    id: id,
  };
};

export const deleteListFail = (data) => {
  return {
    type: actionTypes.DELETE_A_LIST_FAIL,
    data: data,
  };
};
