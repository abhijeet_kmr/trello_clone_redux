import React, { Component } from "react";
import { Link } from "react-router-dom";
import { AddIcon } from "@chakra-ui/icons";
import { Box, Button, Flex, Text, Spinner } from "@chakra-ui/react";
import { connect } from "react-redux";

import {
  createBoardFail,
  createBoardSuccess,
  getBoardDataFail,
  getBoardDataSuccess,
} from "../redux/action";
import * as BoardData from "./apiCalls";
import ModalChakra from "./ModalChakra";

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalIsOpen: false,
      boardTitle: "",
    };
  }

  componentDidMount() {
    this.props.getBoards();
  }

  handleCreateBoardButton = () => {
    this.props.createBoard(this.state.boardTitle).then(() => {
      this.setState({ boardTitle: "", modalIsOpen: false });
    });
  };

  handleInputChange = (e) => {
    this.setState({
      boardTitle: e.target.value,
    });
  };

  onOpen = () => {
    this.setState({ modalIsOpen: true });
  };

  onClose = () => {
    this.setState({ modalIsOpen: false });
  };

  render() {
    console.log(1111, this.props);

    if (this.props.isError) {
      return (
        <Flex h="80vh" justify="center" align="center">
          <Text as="h1" color="red">
            {this.props.isError}
          </Text>
        </Flex>
      );
    }
    if (this.props.isLoading) {
      return (
        <Flex h="80vh" direction="column" justify="center" align="center">
          <Spinner
            thickness="4px"
            speed="0.65s"
            emptyColor="gray.200"
            color="blue.500"
            size="xl"
          />
        </Flex>
      );
    }
    return (
      <Box p="3rem 6rem" bg="#027aa0" minH="95vh">
        <Box>
          <Box>
            <Button
              onClick={this.onOpen}
              boxShadow="base"
              m="1rem"
              p="1rem"
              fontSize="1.5rem"
            >
              Create New Board <AddIcon m="0 .5rem" w={5} h={5} />
            </Button>
          </Box>
          <ModalChakra
            isOpen={this.state.modalIsOpen}
            title={"Board Title"}
            onChangeFn={this.handleInputChange}
            plaeceholder={"Enter Board Title"}
            onClickOnCloseFn={this.onClose}
            createFn={this.handleCreateBoardButton}
            value={this.state.boardTitle}
          />
          <Flex wrap="wrap">
            {this.props.boards.map((board) => {
              return (
                <Link key={board.id} to={`/board/${board.id}`}>
                  <Flex
                    key={board.id}
                    boxShadow="2xl"
                    border="1px dotted #fff"
                    justify="space-between"
                    bg="#026AA7"
                    m="1rem"
                    w="300px"
                    h="100px"
                    borderRadius="1rem"
                  >
                    <Text fontSize="1.5rem" color="white" p=".5rem 1rem">
                      {board.name}
                    </Text>
                  </Flex>
                </Link>
              );
            })}
          </Flex>
        </Box>
      </Box>
    );
  }
}

const mapStateToProps = (state) => {
  console.log(state);
  return {
    boards: state.reducerBoard.boards,
    isLoading: state.reducerBoard.isLoading,
    isError: state.reducerBoard.isError,
  };
};

const mapDispatchToPorps = (dispatch) => ({
  getBoards: () =>
    BoardData.getBoards()
      .then((data) => dispatch(getBoardDataSuccess(data)))
      .catch((err) => dispatch(getBoardDataFail(err))),
  createBoard: (title) =>
    BoardData.createBoard(title)
      .then((data) => dispatch(createBoardSuccess(data)))
      .catch((err) => dispatch(createBoardFail(err))),
});

export default connect(mapStateToProps, mapDispatchToPorps)(Home);
