import React, { Component } from "react";
import { Link } from "react-router-dom";
import { AddIcon, DeleteIcon, ArrowBackIcon } from "@chakra-ui/icons";
import { Box, Button, Flex, Text, Spinner } from "@chakra-ui/react";
import { connect } from "react-redux";

import * as BoardData from "./apiCalls";
import Cards from "./Cards";
import ModalChakra from "./ModalChakra";
import {
  createNewList,
  createNewListFail,
  deleteList,
  deleteListFail,
  getAllLists,
  getAllListsFail,
} from "../redux/action";

class Board extends Component {
  constructor(props) {
    super(props);
    this.state = {
      // allLists: [],
      // cardName: "",
      // listName: "",
      // isLoading: true,
      // isError: false,
      isOpen: false,
      showForm: false,
    };
  }

  componentDidMount = () => {
    const idBoard = this.props.match.params.boardID;
    this.props.allList(idBoard);
  };

  // componentDidMount = () => {
  //   const idBoard = this.props.match.params.boardID;
  //   BoardData.getAllLists(idBoard)
  //     .then((allLists) => {
  //       this.setState({
  //         allLists,
  //         isLoading: false,
  //         isError: false,
  //       });
  //     })
  //     .catch(() => {
  //       this.setState({
  //         allLists: null,
  //         isLoading: false,
  //         isError: "Failed To Load The Lists",
  //       });
  //     });
  // };

  handleCreateList = () => {
    const idBoard = this.props.match.params.boardID;
    this.props.createNewList(this.state.listName, idBoard).then(() => {
      this.setState({
        isOpen: false,
        showForm: false,
        isError: false,
      });
    });
  };

  handleDeleteList = (id) => {
    this.props.deleteList(id, true).then(() => {
      this.setState({
        isError: false,
      });
    });
  };

  handleListName = (e) => {
    const listName = e.target.value;
    this.setState({
      listName: listName,
    });
  };

  onOpen = () => {
    this.setState({
      isOpen: true,
    });
  };

  onClose = () => {
    this.setState({ isOpen: false });
  };

  render() {
    console.log(this.props);

    if (this.props.isError) {
      return (
        <Flex h="80vh" justify="center" align="center">
          <Text as="h1" color="red">
            {this.props.isError}
          </Text>
        </Flex>
      );
    }

    if (this.props.isLoading) {
      return (
        <Flex h="80vh" direction="column" justify="center" align="center">
          <Spinner
            thickness="4px"
            speed="0.65s"
            emptyColor="gray.200"
            color="blue.500"
            size="xl"
          />
        </Flex>
      );
    }

    return (
      <Box>
        <Box bg="#027AA0">
          <Link to="/boards">
            <Button
              boxShadow="xl"
              h="2rem"
              w="6rem"
              m="1rem 0 0 2rem"
              p="0 1rem 0 0"
            >
              <ArrowBackIcon w="1.5rem" /> Back
            </Button>
          </Link>
        </Box>
        <Flex
          overflowX="scroll"
          justify="start"
          align="start"
          bg="#027AA0"
          maxW="100vw"
          minH="100vh"
          whiteSpace="nowrap"
          pb="17px"
          px="32px"
          sx={{
            "::-webkit-scrollbar": {
              display: "none",
            },
          }}
        >
          {this.props.allLists.map((list) => {
            console.log(list);
            return (
              <Flex
                key={list.id}
                direction="column"
                w="350px"
                bg="#D0D0D0"
                m="1rem 1rem 0 0"
                borderRadius="5px"
                p="1rem"
              >
                <Flex justify="space-between">
                  <Text fontWeight="bold" fontSize="2xl">
                    {list.name}
                  </Text>
                  <Button
                    onClick={() => this.handleDeleteList(list.id)}
                    fontWeight="bold"
                  >
                    <DeleteIcon />
                  </Button>
                </Flex>
                <Flex>
                  <Cards id={list.id} />
                </Flex>
              </Flex>
            );
          })}
          <Box>
            <Button
              boxShadow="base"
              m="1rem 0 0 0"
              p="1rem"
              fontSize="1.5rem"
              onClick={this.onOpen}
            >
              Create New List <AddIcon m="0 .5rem" w={5} h={5} />
            </Button>

            <ModalChakra
              isOpen={this.state.isOpen}
              title="List Title"
              placeholder="Enter List Title"
              createFn={this.handleCreateList}
              onChangeFn={this.handleListName}
              onClickOnCloseFn={this.onClose}
            />
          </Box>
        </Flex>
      </Box>
    );
  }
}

const mapStateToProps = (state) => {
  // console.log(state);
  return {
    allLists: state.reducerList.allLists,
    isLoading: state.reducerList.isLoading,
    isError: state.reducerList.isError,
  };
};

const mapDispatchToPorps = (dispatch) => ({
  allList: (boardId) =>
    BoardData.getAllLists(boardId)
      .then((data) => dispatch(getAllLists(data)))
      .catch((err) => dispatch(getAllListsFail(err))),
  createNewList: (listName, idBoard) =>
    BoardData.createNewList(listName, idBoard)
      .then((data) => dispatch(createNewList(data)))
      .catch((err) => dispatch(createNewListFail(err))),
  deleteList: (idList, value) =>
    BoardData.deleteList(idList, value)
      .then((data) => dispatch(deleteList(data, idList)))
      .catch((err) => dispatch(deleteListFail(err))),
});

export default connect(mapStateToProps, mapDispatchToPorps)(Board);
