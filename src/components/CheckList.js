import React, { Component } from "react";
import { CloseIcon, EditIcon, DeleteIcon } from "@chakra-ui/icons";
import {
  Box,
  Button,
  Flex,
  Input,
  Text,
  Checkbox,
  ModalContent,
  ModalHeader,
  ModalBody,
  Progress,
} from "@chakra-ui/react";

import * as BoardData from "./apiCalls";
import checkedCalcFn from "../Functions/checkedCalcFn";

export default class CheckList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      allCheckLists: [],
      isError: false,
      newCheckListName: "",
      newItemName: "",
      checkListPopoverVisibility: false,
      addAnItemButtonVisibility: true,
    };
  }

  componentDidMount = () => {
    BoardData.getAllChecklists(this.props.id)
      .then((response) => {
        this.setState({
          allCheckLists: response,
          isError: false,
        });
      })
      .catch(() => {
        this.setState({
          allCheckLists: null,
          isError: "Failed To Load The CheckLists",
        });
      });
  };

  handleCreateChecklistButton = () => {
    BoardData.createCheckList(this.props.id, this.state.newCheckListName)
      .then((response) => {
        const newAllCheckListsData = [...this.state.allCheckLists, response];
        this.setState({
          allCheckLists: newAllCheckListsData,
          isError: false,
          checkListPopoverVisibility: false,
        });
      })
      .catch(() => {
        this.setState({
          isError: "Failed To Create New Checklist",
        });
      });
  };

  handleCheckListDeleteButton = (idCheckList) => {
    BoardData.deleteCheckList(idCheckList)
      .then(() => {
        const newAllCheckListsData = this.state.allCheckLists.filter(
          (checkList) => checkList.id !== idCheckList
        );
        this.setState({
          allCheckLists: newAllCheckListsData,
          isError: false,
        });
      })
      .catch(() => {
        this.setState({
          isError: "Failed To Delete Checklist",
        });
      });
  };

  handleAddCheckItem = (idCheckList) => {
    BoardData.createCheckItem(idCheckList, this.state.newItemName)
      .then((response) => {
        let obj = {};
        const newAllCheckListsData = this.state.allCheckLists.reduce(
          (acc, curr) => {
            if (curr.id === idCheckList) {
              obj = { ...curr, checkItems: curr.checkItems.concat(response) };
            } else {
              obj = {
                ...curr,
              };
            }
            acc.push(obj);
            return acc;
          },
          []
        );
        this.setState({
          allCheckLists: newAllCheckListsData,
          newItemName: "",
          addAnItemButtonVisibility: true,
          addItemButtonVisibility: false,
          isError: false,
        });
      })
      .catch(() => {
        this.setState({
          isError: "Failed To Create New CheckItem",
        });
      });
  };

  handleDeleteCheckItemButton = (idCheckList, idCheckItem) => {
    BoardData.deleteCheckItem(idCheckList, idCheckItem)
      .then(() => {
        let obj = {};
        const newAllCheckListsData = this.state.allCheckLists.reduce(
          (acc, curr) => {
            const { checkItems, ...rest } = curr;
            if (curr.id === idCheckList) {
              obj = {
                ...rest,
                checkItems: checkItems.filter((el) => el.id !== idCheckItem),
              };
            } else {
              obj = {
                ...curr,
              };
            }
            acc.push(obj);
            return acc;
          },
          []
        );
        this.setState({
          allCheckLists: newAllCheckListsData,
          isError: false,
        });
      })
      .catch((err) => {
        this.setState({
          isError: "Failed To Delete The CheckItem",
        });
      });
  };

  handleAddAnItemButton = () => {
    this.setState({
      addAnItemButtonVisibility: false,
    });
  };

  handleCheckListPopover = () => {
    this.setState({
      checkListPopoverVisibility: !this.state.checkListPopoverVisibility,
    });
  };

  handleCheckListName = (e) => {
    this.setState({
      newCheckListName: e.target.value,
    });
  };

  handleItemName = (e) => {
    this.setState({
      newItemName: e.target.value,
    });
  };

  handleCheckItemStatus = (checked, idCheckItem, idCheckList) => {
    BoardData.updateItemOnCheckList(
      this.props.id,
      idCheckItem,
      checked ? "complete" : "incomplete"
    ).then((response) => {
      const newAllCheckListsData = this.state.allCheckLists.map((checkList) => {
        if (checkList.id === idCheckList) {
          return {
            ...checkList,
            checkItems: checkList.checkItems.map((checkItem) => {
              if (checkItem.id === idCheckItem) {
                return { ...checkItem, state: response.state };
              } else return { ...checkItem };
            }),
          };
        } else return { ...checkList };
      });
      this.setState({ allCheckLists: newAllCheckListsData, isError: false });
    });
  };

  render() {
    const { cardName, handleClose } = this.props;

    if (this.state.isError) {
      return (
        <Flex h="80vh" justify="center" align="center">
          <Text as="h1" color="red">
            {this.state.isError}
          </Text>
        </Flex>
      );
    }

    return (
      <>
        <ModalContent>
          <ModalHeader p="1rem">
            <Flex justify="space-between" align="center">
              <Text fontSize="1.6rem" color="brown">
                {cardName}
              </Text>
              <CloseIcon cursor="pointer" onClick={handleClose} />
            </Flex>
          </ModalHeader>

          <ModalBody>
            <Flex w="100%" justify="end">
              <Button m="0 0 .5rem 0" onClick={this.handleCheckListPopover}>
                <EditIcon m="0 10px" />
                Checklist
              </Button>
            </Flex>
            {this.state.checkListPopoverVisibility ? (
              <Flex w="100%" align="center" p="10px 0">
                <Input
                  m="0 5px 0 0"
                  placeholder="Enter New Checklist"
                  onChange={this.handleCheckListName}
                ></Input>
                <Button
                  colorScheme="teal"
                  w="35%"
                  onClick={this.handleCreateChecklistButton}
                >
                  Create
                </Button>
              </Flex>
            ) : null}

            {this.state.allCheckLists.map((checkList) => {
              return (
                <Flex
                  key={checkList.id}
                  direction="column"
                  bg="#EDEDED"
                  p="10px"
                  m=".5rem 0"
                  border="1px solid #D8D9CF"
                  borderRadius="10px"
                >
                  <Flex
                    justify="space-between"
                    align="center"
                    bg="wheat"
                    p="0 1rem"
                    borderRadius="8px"
                  >
                    <Text fontWeight="bold">
                      <EditIcon m="0 5px" />
                      {checkList.name}
                    </Text>
                    <Button
                      colorScheme="teal"
                      variant="ghost"
                      onClick={() =>
                        this.handleCheckListDeleteButton(checkList.id)
                      }
                    >
                      Delete
                    </Button>
                  </Flex>
                  <Box m="1rem 0">
                    <Text>{checkedCalcFn(checkList.checkItems)}%</Text>
                    <Progress
                      borderRadius="10px"
                      value={checkedCalcFn(checkList.checkItems)}
                    />
                  </Box>
                  <Flex direction="column">
                    {checkList.checkItems.map((checkItem) => {
                      return (
                        <Flex
                          key={checkItem.id}
                          justify="space-between"
                          align="center"
                          bg="#fff"
                          m="0 0 5px 0"
                          p="0 10px"
                          borderRadius="10px"
                        >
                          <Checkbox
                            isChecked={checkItem.state === "complete"}
                            onChange={(e) =>
                              this.handleCheckItemStatus(
                                e.target.checked,
                                checkItem.id,
                                checkList.id
                              )
                            }
                          >
                            <Text>{checkItem.name}</Text>
                          </Checkbox>
                          <Button bg="#fff" borderRadius="50%">
                            <DeleteIcon
                              onClick={() =>
                                this.handleDeleteCheckItemButton(
                                  checkList.id,
                                  checkItem.id
                                )
                              }
                            />
                          </Button>
                        </Flex>
                      );
                    })}
                  </Flex>

                  <Flex align="center">
                    {this.state.addAnItemButtonVisibility ? (
                      <Button
                        colorScheme="teal"
                        m="1rem 0"
                        w="25%"
                        onClick={this.handleAddAnItemButton}
                      >
                        Add an item
                      </Button>
                    ) : null}
                    {!this.state.addAnItemButtonVisibility ? (
                      <Flex
                        justify="space-between"
                        align="center"
                        m="10px 0"
                        w="100%"
                      >
                        <Input
                          m="0 5px 0 0"
                          placeholder="Enter New Item"
                          onChange={this.handleItemName}
                        ></Input>
                        <Button
                          colorScheme="teal"
                          onClick={() => this.handleAddCheckItem(checkList.id)}
                        >
                          Add item
                        </Button>
                      </Flex>
                    ) : null}
                  </Flex>
                </Flex>
              );
            })}
          </ModalBody>
        </ModalContent>
      </>
    );
  }
}
