const checkedCalcFn = (item) => {
  return item.length
    ? Math.floor(
        (item.filter((checkItem) => checkItem.state === "complete").length /
          item.length) *
          100
      )
    : 0;
};

export default checkedCalcFn;
